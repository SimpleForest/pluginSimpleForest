#!/bin/bash

for f in $(find ../pluginsimpleforest/ -name '*.h' -or -name '*.hpp' -or -name '*.cpp');do clang-format -i -style=file $f; done
