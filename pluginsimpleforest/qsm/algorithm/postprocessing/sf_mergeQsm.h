/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_MERGEQSM_H
#define SF_MERGEQSM_H

#include "qsm/model/sf_modelQSM.h"

class SF_MergeQsm
{
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> brickToConnectTo(std::shared_ptr<SF_ModelQSM>& qsm1,
                                                                  std::shared_ptr<SF_ModelQSM>& qsm2);
  void swapIfNeeded(std::shared_ptr<SF_ModelQSM>& qsm1, std::shared_ptr<SF_ModelQSM>& qsm2);
  void setQsm(const std::shared_ptr<SF_ModelQSM>& qsm1, std::shared_ptr<SF_ModelQSM>& qsm2);
  void connect(std::shared_ptr<SF_ModelQSM>& qsm1, std::shared_ptr<SF_ModelQSM>& qsm2);

public:
  SF_MergeQsm() = default;
  std::shared_ptr<SF_ModelQSM> merge(std::shared_ptr<SF_ModelQSM>& qsm1, std::shared_ptr<SF_ModelQSM>& qsm2);
};

#endif // SF_MERGEQSM_H
