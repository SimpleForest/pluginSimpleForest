/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_detectCrown.h"

#include "qsm/model/sf_modelQSM.h"

SF_DetectCrown::SF_DetectCrown() {}

std::shared_ptr<SF_ModelSegment>
SF_DetectCrown::compute(std::shared_ptr<SF_ModelQSM> qsm)
{
  std::shared_ptr<SF_ModelSegment> stemSegment = qsm->getRootSegment();
  std::shared_ptr<SF_ModelSegment> belowCrownSegment = stemSegment;
  float maxGrowthLengthBranches = 0;
  while (stemSegment->getChildren().size() > 0) {
    std::vector<std::shared_ptr<SF_ModelSegment>> children = stemSegment->getChildren();
    float currentGrowthLength = 0;
    for (size_t i = 1; i < children.size(); i++) {
      std::shared_ptr<SF_ModelSegment> child = children.at(i);
      currentGrowthLength += child->getBuildingBricks().front()->getGrowthLength();
    }
    if (currentGrowthLength > maxGrowthLengthBranches) {
      maxGrowthLengthBranches = currentGrowthLength;
      stemSegment = stemSegment;
    }
    stemSegment = children.at(0);
  }
  return stemSegment;
}
