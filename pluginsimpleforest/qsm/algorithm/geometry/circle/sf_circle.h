/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_CIRCLE_H
#define SF_CIRCLE_H

#include "pcl/common/geometry.h"
#include "pcl/sf_math.h"
#include "pcl/sf_point.h"
#include "steps/param/sf_paramAllSteps.h"

#include <utility>

template<typename PointType>
class SF_Circle
{
  std::int32_t m_maxIterations;
  const SF_SphereFollowingParameters& m_params;
  pcl::ModelCoefficients m_coeff;
  std::vector<float> m_possibleIterations;
  pcl::SACSegmentation<PointType> m_segmentation;
  pcl::ModelCoefficients cirlceMedian(typename pcl::PointCloud<PointType>::Ptr& cloudIn);
  pcl::ModelCoefficients cirlceSACModel(typename pcl::PointCloud<PointType>::Ptr& cloudIn);
  pcl::ModelCoefficients cirlceLeastSqaresModel(typename pcl::PointCloud<PointType>::Ptr& cloudIn);
  void setCoeff(pcl::ModelCoefficients& circleMedian, pcl::ModelCoefficients& circleSACModel);
  bool sacInRangeOfMedian(pcl::ModelCoefficients& circleMedian, pcl::ModelCoefficients& circleSACModel) const;
  void setMaxIterations(typename pcl::PointCloud<PointType>::Ptr& cloudIn);

public:
  SF_Circle(const SF_SphereFollowingParameters& params);
  void compute(typename pcl::PointCloud<PointType>::Ptr& cloudIn);
  pcl::ModelCoefficients coeff() const;
  std::int32_t maxIterations() const;
};

#include "sf_circle.hpp"

#endif // SF_CIRCLE_H
