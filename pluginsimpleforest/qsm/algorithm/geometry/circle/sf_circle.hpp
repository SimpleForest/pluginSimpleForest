/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_CIRCLE_HPP
#define SF_CIRCLE_HPP

#include "sf_circle.h"

#include <pcl/common/centroid.h>
#include <pcl/common/common.h>

#include <cmath>

#include <math/fit/circle/sf_fit3dCircle.hpp>

template<typename PointType>
SF_Circle<PointType>::SF_Circle(const SF_SphereFollowingParameters& params) : m_params(params)
{
  m_segmentation.setOptimizeCoefficients(true);
  m_segmentation.setModelType(pcl::SACMODEL_CIRCLE3D);
  m_segmentation.setMethodType(m_params._fittingMethod);
  m_segmentation.setDistanceThreshold(m_params._inlierDistance);
  // Not parameterized. Hopefully enough.
  std::int32_t size{ 50 };
  m_possibleIterations.reserve(size);
  for (auto index = 0; index < size; ++index) {
    std::int32_t sparse = static_cast<std::uint32_t>(std::round(std::pow(index, 1.5)));
    sparse = std::max(sparse, 5);
    sparse = std::min(sparse, m_params._RANSACIterations);
    m_possibleIterations.push_back(sparse);
  }
}
template<typename PointType>
void
SF_Circle<PointType>::compute(typename pcl::PointCloud<PointType>::Ptr& cloudIn)
{
  pcl::ModelCoefficients circleMedian = cirlceMedian(cloudIn);
  pcl::ModelCoefficients circleModel = m_params.m_useLeastSquares ? cirlceLeastSqaresModel(cloudIn) : cirlceSACModel(cloudIn);
  setCoeff(circleMedian, circleModel);
}
template<typename PointType>
void
SF_Circle<PointType>::setMaxIterations(typename pcl::PointCloud<PointType>::Ptr& cloudIn)
{
  const auto possibleIterationsSize = m_possibleIterations.size();
  const auto cloudSize = cloudIn->points.size();
  const auto iterations = cloudSize >= possibleIterationsSize ? m_params._RANSACIterations : m_possibleIterations[cloudSize];
  m_segmentation.setMaxIterations(iterations);
}

template<typename PointType>
pcl::ModelCoefficients
SF_Circle<PointType>::cirlceMedian(typename pcl::PointCloud<PointType>::Ptr& cloudIn)
{
  Eigen::Vector4f centroid;
  pcl::compute3DCentroid(*cloudIn, centroid);
  std::vector<float> distances;
  distances.reserve(cloudIn->points.size());
  std::transform(cloudIn->points.cbegin(), cloudIn->points.cend(), std::back_inserter(distances), [&centroid](const PointType& point) {
    Eigen::Vector3f diff(point.x - centroid[0], point.y - centroid[1], point.z - centroid[2]);
    return diff.norm();
  });
  pcl::ModelCoefficients circleMedian;
  circleMedian.values.reserve(4);
  circleMedian.values.push_back(centroid[0]);
  circleMedian.values.push_back(centroid[1]);
  circleMedian.values.push_back(centroid[2]);
  circleMedian.values.push_back(SF_Math<float>::getMedian(distances));
  return circleMedian;
}

template<typename PointType>
pcl::ModelCoefficients
SF_Circle<PointType>::cirlceSACModel(typename pcl::PointCloud<PointType>::Ptr& cloudIn)
{
  setMaxIterations(cloudIn);
  m_segmentation.setInputCloud(cloudIn);
  pcl::ModelCoefficients coeff;
  pcl::PointIndices inliersCylinder;
  m_segmentation.segment(inliersCylinder, coeff);
  return coeff;
}

template<typename PointType>
pcl::ModelCoefficients
SF_Circle<PointType>::cirlceLeastSqaresModel(typename pcl::PointCloud<PointType>::Ptr& cloudIn)
{
  SF_Fit3dCircle<PointType> fit(cloudIn);
  try {
    fit.compute();
    return fit.circle();

  } catch (...) {
    fit.compute();
    return fit.errorCircle();
  }
}

template<typename PointType>
pcl::ModelCoefficients
SF_Circle<PointType>::coeff() const
{
  return m_coeff;
}

template<typename PointType>
void
SF_Circle<PointType>::setCoeff(pcl::ModelCoefficients& circleMedian, pcl::ModelCoefficients& circleSACModel)
{
  if (circleSACModel.values.size() == 7) {
    circleSACModel.values.resize(4);
  }
  if (circleSACModel.values.size() == 4 && sacInRangeOfMedian(circleMedian, circleSACModel)) {
    m_coeff = circleSACModel;
    return;
  }
  m_coeff = circleMedian;
}

template<typename PointType>
bool
SF_Circle<PointType>::sacInRangeOfMedian(pcl::ModelCoefficients& circleMedian, pcl::ModelCoefficients& circleSACModel) const
{
  return circleSACModel.values[3] > circleMedian.values[3] * 0.5 &&
         circleSACModel.values[3] < circleMedian.values[3] * m_params._medianRadiusMultiplier;
}

template<typename PointType>
std::int32_t
SF_Circle<PointType>::maxIterations() const
{
  return m_segmentation.getMaxIterations();
}

#endif // SF_CIRCLE_HPP
