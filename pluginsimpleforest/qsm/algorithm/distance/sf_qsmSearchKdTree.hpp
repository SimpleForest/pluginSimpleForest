/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_QSMSEARCHKDTREE_HPP
#define SF_QSMSEARCHKDTREE_HPP

#include "sf_qsmSearchKdTree.h"

#include "pcl/sf_math.h"

template<typename PointType>
void
SF_QSMSearchKdtree<PointType>::initialize()
{
  m_buildingBricks = m_qsm->getBuildingBricks();
  initializeKdTree();
  const auto size = static_cast<int>(m_buildingBricks.size());
  m_k = std::min(m_k, size);
  if (m_method != GROWTHDISTANCE) {
    return;
  }
  m_growthLengths.resize(size);
  size_t id = 0;
  std::for_each(m_buildingBricks.begin(), m_buildingBricks.end(), [this, &id](std::shared_ptr<Sf_ModelAbstractBuildingbrick>& brick) {
    m_growthLengths[id] = std::max(brick->getGrowthLength(), m_minGrowthLength);
    brick->setID(id++);
  });
}

template<typename PointType>
double
SF_QSMSearchKdtree<PointType>::distance(const PointType& point, const std::shared_ptr<Sf_ModelAbstractBuildingbrick>& brick)
{
  if (m_useAngle) {
    Eigen::Vector3d normal(point.normal_x, point.normal_y, point.normal_z);
    auto angle = SF_Math<double>::getAngleBetweenDeg(normal, brick->getAxis());
    if (angle < m_minAngle) {
      if (m_method == GROWTHDISTANCE) {
        return -m_minGrowthLength;
      }
      return m_errorDistance;
    }
  }
  if (m_method == GROWTHDISTANCE) {
    return -m_growthLengths[brick->getID()];
  } else if (m_method == DISTANCETOAXISSEGMENT) {
    return brick->getDistanceIfOnAxis(point);
  }
  return std::abs(brick->getDistanceIfOnAxis(point) - brick->getRadius());
}

template<typename PointType>
std::pair<double, int>
SF_QSMSearchKdtree<PointType>::distance(const PointType& point)
{
  std::vector<int> pointIdxRadiusSearch(m_k);
  std::vector<float> pointRadiusSquaredDistance(m_k);
  double minDistance = m_errorDistance / 2;
  int bestIndex = -1;
  if (m_kdTreeQSM->nearestKSearch(point, m_k, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0) {
    for (size_t j = 0; j < pointIdxRadiusSearch.size(); ++j) {
      auto index = pointIdxRadiusSearch[j];
      auto dist = distance(point, m_buildingBricks[index]);
      if (dist < minDistance) {
        bestIndex = index;
        minDistance = dist;
      }
    }
  } else {
    throw std::runtime_error("SF_QSMSearchKdtree<PointType, method, true>::distance(const PointType &point) error:");
  }
  return std::make_pair(minDistance, bestIndex);
}

template<typename PointType>
void
SF_QSMSearchKdtree<PointType>::initializeKdTree()
{
  m_kdTreeQSM.reset(new typename pcl::KdTreeFLANN<PointType>());
  typename pcl::PointCloud<PointType>::Ptr centerCloud(new typename pcl::PointCloud<PointType>());
  auto size = m_buildingBricks.size();
  centerCloud->points.resize(size);
  centerCloud->width = size;
  centerCloud->height = 1;
  for (size_t i = 0; i < size; i++) {
    auto center = m_buildingBricks[i]->getCenter();
    PointType p;
    p.getVector3fMap() = center.cast<float>();
    centerCloud->points[i] = std::move(p);
  }
  m_kdTreeQSM->setInputCloud(centerCloud);
}

template<typename PointType>
SF_QSMSearchKdtree<PointType>::SF_QSMSearchKdtree(std::shared_ptr<SF_ModelQSM> qsm, SF_CloudToModelDistanceParameters& params)
  : m_qsm(qsm), m_method(params._method), m_useAngle(params.m_useAngle), m_maxDistance(params._inlierDistance), m_k(params._k)
{
  initialize();
}

#endif // SF_QSMSEARCHKDTREE_HPP
