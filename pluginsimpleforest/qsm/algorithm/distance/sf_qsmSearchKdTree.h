/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_QSMSEARCHKDTREE_H
#define SF_QSMSEARCHKDTREE_H

#include "sf_qsmSearchKdTreeInterface.h"

template<typename PointType>
class SF_QSMSearchKdtree : public SF_QSMSearchKdtreeInterface<PointType>
{
  std::shared_ptr<SF_ModelQSM> m_qsm;
  SF_CLoudToModelDistanceMethod m_method;
  bool m_useAngle; // TODO check this if it is still needed
  double m_maxDistance;
  const double m_minGrowthLength = 0.001;
  const double m_errorDistance = std::numeric_limits<double>::max();
  double m_minAngle;
  int m_k;
  typename pcl::KdTreeFLANN<PointType>::Ptr m_kdTreeQSM;
  std::vector<double> m_growthLengths;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> m_buildingBricks;
  void initialize();
  void initializeKdTree();
  double distance(const PointType& point, const std::shared_ptr<Sf_ModelAbstractBuildingbrick>& brick);

public:
  SF_QSMSearchKdtree(std::shared_ptr<SF_ModelQSM> qsm, SF_CloudToModelDistanceParameters& params);
  std::pair<double, int> distance(const PointType& point) override;
};

#include "sf_qsmSearchKdTree.hpp"

#endif // SF_QSMSEARCHKDTREE_H
