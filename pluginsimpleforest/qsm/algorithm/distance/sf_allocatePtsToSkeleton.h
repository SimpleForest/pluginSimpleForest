/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_ALLOCATEPTSTOSKELETON_H
#define SF_ALLOCATEPTSTOSKELETON_H

#include "sf_qsmSearchKdTree.h"

template<typename PointType>
class SF_AllocatePointsToSkeleton
{
protected:
  std::shared_ptr<SF_ModelQSM> m_qsm;
  SF_CloudToModelDistanceParameters m_params;
  typename pcl::PointCloud<PointType>::Ptr m_cloud;
  std::shared_ptr<SF_QSMSearchKdtree<PointType>> m_qsmSearch;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> m_buildingBricks;
  std::vector<typename pcl::PointCloud<PointType>::Ptr> m_brickClusters;
  void initialize();
  void compute();

public:
  SF_AllocatePointsToSkeleton(std::shared_ptr<SF_ModelQSM> qsm,
                              SF_CloudToModelDistanceParameters& params,
                              typename pcl::PointCloud<PointType>::Ptr cloud);
  std::vector<typename pcl::PointCloud<PointType>::Ptr> brickClusters() const;
};

#include "sf_allocatePtsToSkeleton.hpp"

#endif // SF_ALLOCATEPTSTOSKELETON_H
