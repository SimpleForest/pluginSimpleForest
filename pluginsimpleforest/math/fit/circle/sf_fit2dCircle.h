/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_FITCIRCLE_H
#define SF_FITCIRCLE_H

#include "pcl/sf_math.h"

template<typename T>
struct SF_Point2d
{
  T x;
  T y;
};

template<typename T>
struct SF_Circle2d
{
  SF_Point2d<T> center;
  T radius;
};

template<typename T>
class SF_Fit2dCircle
{
protected:
  SF_Circle2d<T> m_circle;
  std::uint32_t m_gaussNewtonIterations;
  T m_minDelta;
  std::vector<T> m_x;
  std::vector<T> m_y;
  Eigen::MatrixXd getJacobian(const std::vector<T>& xVec, const std::vector<T>& yVec);
  Eigen::MatrixXd getResiduals(const std::vector<T>& xVec, const std::vector<T>& yVec);
  Eigen::MatrixXd getJacobianInitial(const std::vector<T>& xVec, const std::vector<T>& yVec);
  Eigen::MatrixXd getResidualsInitial(const std::vector<T>& xVec, const std::vector<T>& yVec);

public:
  SF_Fit2dCircle();
  SF_Fit2dCircle(std::uint32_t iterations, T minDelta);
  std::vector<T> x() const;
  std::vector<T> y() const;
  void setY(const std::vector<T>& y);
  void setX(const std::vector<T>& x);
  void compute();
  std::vector<T> circle();
  std::vector<T> errorCircle() const;
};

#include "math/fit/circle/sf_fit2dCircle.hpp"

#endif // SF_FITCIRCLE_H
