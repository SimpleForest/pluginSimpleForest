/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_QSMALLOMETRICCHECK_H
#define SF_QSMALLOMETRICCHECK_H

#include "steps/qsm/sf_abstractStepQSM.h"

#include <QString>

class SF_StepQSMAllometricCorrection : public SF_AbstractStepQSM
{
  Q_OBJECT

public:
  SF_StepQSMAllometricCorrection(CT_StepInitializeData& dataInit);
  ~SF_StepQSMAllometricCorrection();
  QString getStepDescription() const;
  QString getStepDetailledDescription() const;
  QString getStepURL() const;
  CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData& dataInit);
  QStringList getStepRISCitations() const;

protected:
  void createInResultModelListProtected();
  void createOutResultModelListProtected();
  void createParamList(CT_ResultGroup* out_result);
  virtual void createPreConfigurationDialog() {}
  virtual void createPostConfigurationDialog();
  void compute();
  QList<SF_ParamAllometricCorrectionNeighboring> _paramList;
  void createPostConfigurationDialogCitationSecond(CT_StepConfigurableDialog* configDialog);

private:
  CT_AutoRenameModels m_outCloudItem;
  CT_AutoRenameModels _outCylinderGroup;
  CT_AutoRenameModels _outCylinders;
  CT_AutoRenameModels _outSFQSM;
  int toStringSFMethod();
  SF_CLoudToModelDistanceMethod toStringCMDMethod();

  double _range = 0.15;
  double _minRadius = 0.0025;
  bool m_estimateMinRadius = false;
  QString m_growthVolume = "GrowthVolume";
  QString m_growthLength = "GrowthLength";
  QString m_vesselVolume = "VesselVolume";
  QString m_growthParameterSelected = m_vesselVolume;
  QStringList m_growthParameters;
  double m_power = 1.0 / 2.49;
  bool m_withIntercept = false;
  double m_quantile = 0.5;
  int m_minPts = 5;
  double m_inlierDistance = 0.02;
  int m_ransacIterations = 1000;
  int m_gaussNewtonIterations = 20;
  bool m_estimateParams = true;
};

#endif // SF_QSMALLOMETRICCHECK_H
