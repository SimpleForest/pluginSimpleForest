/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPQSMALLOMETRICCORRECTIONMANUALADAPTER_H
#define SF_STEPQSMALLOMETRICCORRECTIONMANUALADAPTER_H

#include <QThreadPool>

#include "qsm/algorithm/postprocessing/sf_qsmAllometricCorrection.h"
#include "steps/param/sf_paramAllSteps.h"

class SF_QSMAllometricCorrectionManualAdapter
{
public:
  std::shared_ptr<QMutex> mMutex;

  SF_QSMAllometricCorrectionManualAdapter(const SF_QSMAllometricCorrectionManualAdapter& obj) { mMutex = obj.mMutex; }

  SF_QSMAllometricCorrectionManualAdapter() { mMutex.reset(new QMutex); }

  ~SF_QSMAllometricCorrectionManualAdapter() {}

  void operator()(SF_ParamAllometricCorrectionNeighboring& params)
  {
    SF_ParamAllometricCorrectionNeighboring paramsCpy;
    {
      QMutexLocker m1(&*mMutex);
      paramsCpy = params;
    }
    SF_QSMAllometricCorrection ac;
    {
      QMutexLocker m1(&*mMutex);
      paramsCpy._qsm->sort(SF_ModelSegment::SF_SORTTYPE::GROWTH_VOLUME);
      ac.setParams(paramsCpy);
    }
    ac.compute();
    {
      QMutexLocker m1(&*mMutex);
      params = paramsCpy;
    }
  }
};

#endif // SF_STEPQSMALLOMETRICCORRECTIONMANUALADAPTER_H
