/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPREVERSEPIPEMODELCORRECTIONADAPTER_H
#define SF_STEPREVERSEPIPEMODELCORRECTIONADAPTER_H

#include <QThreadPool>

#include "qsm/algorithm/postprocessing/sf_QSMInversePipeModelParamaterEstimation.h"
#include "steps/param/sf_paramAllSteps.h"

class SF_QSMReversePipeModelCorrectionAdapter
{
public:
  std::shared_ptr<QMutex> mMutex;

  SF_QSMReversePipeModelCorrectionAdapter(const SF_QSMReversePipeModelCorrectionAdapter& obj) { mMutex = obj.mMutex; }

  SF_QSMReversePipeModelCorrectionAdapter() { mMutex.reset(new QMutex); }

  ~SF_QSMReversePipeModelCorrectionAdapter() {}

  void operator()(SF_ParamReversePipeModelCorrection& params)
  {
    SF_ParamReversePipeModelCorrection paramsCpy;
    {
      QMutexLocker m1(&*mMutex);
      paramsCpy = params;
    }
    SF_QSMReversePipeModelParamaterEstimation paramEst;
    {
      QMutexLocker m1(&*mMutex);
      paramEst.setParams(paramsCpy);
    }
    paramEst.compute();
    {
      auto equation = paramEst.equation();
      auto bricks = paramsCpy._qsm->getBuildingBricks();
      for (auto brick : bricks) {
        if (brick->getSegment()->isRoot()) {
          continue;
        }
        if (equation.first == 0) {
          continue;
        }
        auto radius = brick->getRadius();
        auto reverseBranchOrder = brick->getSegment()->getReversePipeBranchOrder();
        auto predictedRadius = equation.first * reverseBranchOrder + equation.second;
        if (predictedRadius <= 0.001) {
          predictedRadius = 0.001;
        }
        auto deviation = paramsCpy.m_range * predictedRadius;
        float minX = predictedRadius - deviation;
        float maxX = predictedRadius + deviation;
        if (radius > minX && radius < maxX) {
          continue;
        }
        brick->setRadius(predictedRadius, FittingType::REVERSEPIPEMODEL);
      }
    }
    {
      QMutexLocker m1(&*mMutex);
      paramsCpy._qsm->sort(SF_ModelSegment::SF_SORTTYPE::GROWTH_VOLUME);
      params = paramsCpy;
    }
  }
};

#endif // SF_STEPREVERSEPIPEMODELCORRECTIONADAPTER_H
