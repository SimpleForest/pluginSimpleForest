/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_qsmItem.h"

SF_QSMItem::SF_QSMItem() {}

SF_QSMItem::SF_QSMItem(const CT_OutAbstractSingularItemModel* model, const CT_AbstractResult* result, std::shared_ptr<SF_ModelQSM> qsm)
  : CT_AbstractItemDrawableWithoutPointCloud(model, result)
{
  if (!qsm)
    return;
  _qsm = qsm->clone();
}

SF_QSMItem::SF_QSMItem(const QString& modelName, const CT_AbstractResult* result, std::shared_ptr<SF_ModelQSM> qsm)
  : CT_AbstractItemDrawableWithoutPointCloud(modelName, result)
{
  if (!qsm)
    return;
  _qsm = qsm->clone();
}

CT_AbstractItemDrawable*
SF_QSMItem::copy(const CT_OutAbstractItemModel* model, const CT_AbstractResult* result, CT_ResultCopyModeList)
{
  SF_QSMItem* ref = new SF_QSMItem((const CT_OutAbstractSingularItemModel*)model, result, _qsm);
  ref->setAlternativeDrawManager(getAlternativeDrawManager());
  return ref;
}

CT_AbstractItemDrawable*
SF_QSMItem::copy(const QString& modelName, const CT_AbstractResult* result, CT_ResultCopyModeList)
{
  SF_QSMItem* ref = new SF_QSMItem(modelName, result, _qsm);
  ref->setAlternativeDrawManager(getAlternativeDrawManager());
  return ref;
}

QString
SF_QSMItem::getTreeID() const
{
  return _id.completeName();
}

std::shared_ptr<SF_ModelQSM>
SF_QSMItem::getQsm() const
{
  return _qsm;
}
