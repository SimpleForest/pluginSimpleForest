/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_stepDTM.h"

#include <pcl/features/normal_3d.h>

#include "converters/CT_To_PCL/sf_converterCTToPCL.h"
#include "converters/CT_To_PCL/sf_converterCTToPCLDTM.h"
#include "pcl/filters/voxel_grid.h"
#include "pcl/geometry/DTM/sf_dtm.h"

SF_StepDTM::SF_StepDTM(CT_StepInitializeData& dataInit) : SF_AbstractStep(dataInit)
{
  _pointDensities.clear();
  _pointDensities.append(m_less);
  _pointDensities.append(m_intermediate);
  _pointDensities.append(m_many);
}

SF_StepDTM::~SF_StepDTM() {}

QString
SF_StepDTM::getStepDescription() const
{
  return tr("Dtm Pyramidal Mlesac Fit");
}

QString
SF_StepDTM::getStepDetailledDescription() const
{
  return tr("Dtm Pyramidal Mlesac Fit - uses a pyramidal paralell plane fitting approach"
            " on a robust downscaled input ground cloud.");
}

QString
SF_StepDTM::getStepURL() const
{
  return tr("http://simpleforest.org/");
}

CT_VirtualAbstractStep*
SF_StepDTM::createNewInstance(CT_StepInitializeData& dataInit)
{
  return new SF_StepDTM(dataInit);
}

QStringList
SF_StepDTM::getStepRISCitations() const
{
  QStringList _risCitationList;
  _risCitationList.append(getRISCitationSimpleTree());
  _risCitationList.append(getRISCitationPCL());
  return _risCitationList;
}

void
SF_StepDTM::createInResultModelListProtected()
{
  CT_InResultModelGroupToCopy* resModel = createNewInResultModelForCopy(DEF_IN_RESULT, tr("Result ground"));
  assert(resModel != NULL);
  resModel->setZeroOrMoreRootGroup();
  resModel->addGroupModel("",
                          DEF_IN_GRP_CLUSTER,
                          CT_AbstractItemGroup::staticGetType(),
                          tr("Ground group"),
                          "",
                          CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  resModel->addItemModel(DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED, CT_Scene::staticGetType(), tr("Ground cloud"));
  resModel->addGroupModel(
    "", DEF_IN_SCENE, CT_AbstractItemGroup::staticGetType(), tr("Root group"), "", CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
  // resModel->addItemModel(DEF_IN_SCENE, DEF_IN_SCENE_CLOUD, CT_Scene::staticGetType(), tr("Root cloud"));
}

void
SF_StepDTM::createPostConfigurationDialogExpert(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addDouble("First the cloud is downscaled to a [<em><b>voxel size</b></em>] of ", " (m).", 0.015, 0.1, 3, m_voxelSize);
  configDialog->addDouble("For each of the downscaled points its normal is"
                          " computed with a [<em><b>search range</b></em>] of ",
                          " (m). ",
                          0.025,
                          0.5,
                          3,
                          m_radiusNormal);
  configDialog->addDouble("The final [<em><b>cell size</b></em>] of the DTM is supposed to be ", " (m).", 0.025, 0.5, 3, m_cellSize);
  configDialog->addDouble("The [<em><b>angle</b></em>] between a plane normal and the parent"
                          " plane normal has to be smaller than ",
                          " (°).",
                          0.5,
                          45,
                          1,
                          m_angle);
  //  configDialog->addInt("For IDW interpolation the following number of [<em><b>nearest neighbors IDW</b></em>]"
  //                       " is needed ",
  //                       ".",
  //                       1,
  //                       99,
  //                       m_idwNeighbors);
  //  configDialog->addInt("For median interpolation the following number of"
  //                       " [<em><b>nearest neighbors median</b></em>] is needed ",
  //                       ".",
  //                       1,
  //                       99,
  //                       m_medianNeighbors);
}

void
SF_StepDTM::createPostConfigurationDialogBeginner(CT_StepConfigurableDialog* configDialog)
{
  configDialog->addStringChoice("Choose the slope of the terrain", "", _pointDensities, m_choicePointDensity);
}

void
SF_StepDTM::createOutResultModelListProtected()
{
  CT_OutResultModelGroupToCopyPossibilities* resModelw = createNewOutResultModelToCopy(DEF_IN_RESULT);
  if (resModelw != NULL) {
    resModelw->addGroupModel(DEF_IN_SCENE, m_outGroundGRP, new CT_StandardItemGroup(), tr("Terrain"));
    resModelw->addItemModel(m_outGroundGRP, m_outDTM, new CT_Image2D<float>(), tr("Dtm"));
    resModelw->addItemModel(m_outGroundGRP, m_outCloud, new CT_Scene(), tr("Ground points"));
  }
}

void
SF_StepDTM::adaptParametersToExpertLevel()
{
  m_radiusNormal = std::max(3 * m_voxelSize, m_radiusNormal);
  if (!_isExpert) {
    if (m_choicePointDensity == m_less) {
      m_angle = 10;
      m_radiusNormal = 0.15;
      m_voxelSize = 0.05;
    } else if (m_choicePointDensity == m_intermediate) {
      m_angle = 20;
      m_radiusNormal = 0.15;
      m_voxelSize = 0.05;
    } else {
      m_angle = 40;
      m_radiusNormal = 0.15;
      m_voxelSize = 0.05;
    }
    m_cellSize = 0.2;
    m_medianNeighbors = 9;
    m_idwNeighbors = 3;
  }
}

void
SF_StepDTM::copyCroppedHeights(pcl::PointCloud<pcl::PointXYZINormal>::Ptr groundCloud,
                               std::shared_ptr<CT_Image2D<float>> dtmPtr,
                               CT_Image2D<float>* CTDTM)
{
  float minZ = std::numeric_limits<float>::max();
  float maxz = std::numeric_limits<float>::lowest();
  for (size_t i = 0; i < groundCloud->points.size(); i++) {
    pcl::PointXYZINormal p = groundCloud->points[i];
    if (p.z < minZ)
      minZ = p.z;
    if (p.z > maxz)
      maxz = p.z;
  }

  for (size_t i = 0; i < CTDTM->xArraySize(); i++) {
    for (size_t j = 0; j < CTDTM->yArraySize(); j++) {
      size_t indexCTDTM{};
      CTDTM->index(i, j, indexCTDTM);
      Eigen::Vector2d bot{ 0, 0 }, top{ 0, 0 }, center{ 0, 0 };
      CTDTM->getCellCoordinates(indexCTDTM, bot, top);
      center[0] = (bot[0] + top[0]) / 2 - m_translate(0);
      center[1] = (bot[1] + top[1]) / 2 - m_translate(1);
      size_t indexdtmPtr{};
      dtmPtr->indexAtCoords(center[0], center[1], indexdtmPtr);

      float height = dtmPtr->valueAtIndex(indexdtmPtr);
      if (height < minZ)
        height = minZ;
      if (height > maxz)
        height = maxz;

      CTDTM->setValueAtIndex(indexCTDTM, height + m_translate(2));
    }
  }
}

void
SF_StepDTM::compute()
{
  m_computationsDone = 0;
  adaptParametersToExpertLevel();
  const QList<CT_ResultGroup*>& out_result_list = getOutResultList();
  CT_ResultGroup* out_result = out_result_list.at(0);
  identifyAndRemoveCorruptedScenes(out_result);
  CT_ResultGroupIterator iter(out_result, this, DEF_IN_SCENE);
  CT_StandardItemGroup* root = (CT_StandardItemGroup*)iter.next();
  CT_StandardItemGroup* terrainGrp = new CT_StandardItemGroup(m_outGroundGRP.completeName(), out_result);
  root->addGroup(terrainGrp);
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr groundCloud = createGroundCloud(out_result, terrainGrp);
  SF_DTM<pcl::PointXYZINormal> sfDTM(groundCloud, m_angle, m_cellSize, out_result, m_outDTMDummy);
  std::shared_ptr<CT_Image2D<float>> dtmPtr = sfDTM.DTM();
  CT_Image2D<float>* dtm = CT_Image2D<float>::createImage2DFromXYCoords(m_outDTMDummy.completeName(),
                                                                        out_result,
                                                                        dtmPtr->minX() + m_translate(0),
                                                                        dtmPtr->minY() + m_translate(1),
                                                                        dtmPtr->maxX() + m_translate(0),
                                                                        dtmPtr->maxY() + m_translate(1),
                                                                        dtmPtr->resolution(),
                                                                        m_translate(2),
                                                                        1337,
                                                                        0);
  copyCroppedHeights(groundCloud, dtmPtr, dtm);
  SF_ConverterCTToPCLDTM dtmConverter(m_translate, dtm);
  std::shared_ptr<SF_ModelDTM> dtmModel = dtmConverter.dtmPCL();

  CT_Image2D<float>* dtmTrueResolution = CT_Image2D<float>::createImage2DFromXYCoords(m_outDTMDummy.completeName(),
                                                                                      out_result,
                                                                                      dtmPtr->minX() + m_translate(0),
                                                                                      dtmPtr->minY() + m_translate(1),
                                                                                      dtmPtr->maxX() + m_translate(0),
                                                                                      dtmPtr->maxY() + m_translate(1),
                                                                                      m_radiusNormal,
                                                                                      m_translate(2),
                                                                                      1337,
                                                                                      0);
  SF_ConverterCTToPCLDTM dtmConverterTrueResolution(m_translate, dtmTrueResolution);
  std::shared_ptr<SF_ModelDTM> dtmModelTrueResolution = dtmConverterTrueResolution.dtmPCL();
  dtmModel->interpolateIDW(m_idwNeighbors, dtmModelTrueResolution);
  CT_Image2D<float>* dtmMedianSmoothed = CT_Image2D<float>::createImage2DFromXYCoords(m_outDTM.completeName(),
                                                                                      out_result,
                                                                                      dtmPtr->minX() + m_translate(0),
                                                                                      dtmPtr->minY() + m_translate(1),
                                                                                      dtmPtr->maxX() + m_translate(0),
                                                                                      dtmPtr->maxY() + m_translate(1),
                                                                                      m_radiusNormal,
                                                                                      m_translate(2),
                                                                                      1337,
                                                                                      0);
  SF_ConverterCTToPCLDTM dtmConverterMedianSmoothed(m_translate, dtmMedianSmoothed);
  std::shared_ptr<SF_ModelDTM> dtmModelMedianSmoothed = dtmConverterMedianSmoothed.dtmPCL();
  dtmModelTrueResolution->interpolateMedian(m_medianNeighbors, dtmModelMedianSmoothed);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = dtmModelMedianSmoothed->getCloud3D();
  for (size_t i = 0; i < dtmMedianSmoothed->nCells(); i++) {
    dtmMedianSmoothed->setValueAtIndex(i, cloud->points[i].z + m_translate(2));
  }
  dtmMedianSmoothed->computeMinMax();
  terrainGrp->addItemDrawable(dtmMedianSmoothed);
  CT_Scene* scene = mergeIndices(out_result, terrainGrp, DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED);
  addGroundCloudToResult(scene, terrainGrp, out_result);
  delete dtmTrueResolution;
  delete dtm;
  writeLogger();
}

void
SF_StepDTM::writeLogger()
{
  QString str = "The DTM was modelled with a cell size of ";
  str.append(QString::number(m_radiusNormal));
  str.append(" (m).");
  PS_LOG->addMessage(LogInterface::info, LogInterface::step, str);
}

void
SF_StepDTM::computeNormals(pcl::PointCloud<pcl::PointXYZINormal>::Ptr downscaledCloud)
{
  pcl::NormalEstimation<pcl::PointXYZINormal, pcl::PointXYZINormal> ne;
  ne.setInputCloud(downscaledCloud);
  pcl::search::KdTree<pcl::PointXYZINormal>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZINormal>());
  ne.setSearchMethod(tree);
  ne.setRadiusSearch(m_radiusNormal);
  ne.compute(*downscaledCloud);
}

pcl::PointCloud<pcl::PointXYZINormal>::Ptr
SF_StepDTM::downScale(pcl::PointCloud<pcl::PointXYZINormal>::Ptr cloud)
{
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr downscaledCloud(new pcl::PointCloud<pcl::PointXYZINormal>);
  pcl::VoxelGrid<pcl::PointXYZINormal> sor;
  sor.setInputCloud(cloud);
  sor.setLeafSize(m_voxelSize, m_voxelSize, m_voxelSize);
  sor.filter(*downscaledCloud);
  return downscaledCloud;
}

pcl::PointCloud<pcl::PointXYZINormal>::Ptr
SF_StepDTM::convert(CT_Scene* scene)
{
  Sf_ConverterCTToPCL<pcl::PointXYZINormal> converter;
  converter.setItemCpyCloudInDeprecated(scene);
  converter.compute();
  m_translate = converter.translation();
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr cloud = converter.cloudTranslated();
  return cloud;
}

CT_Scene*
SF_StepDTM::addGroundCloudToResult(CT_Scene* scene, CT_StandardItemGroup* root, CT_ResultGroup*)
{
  scene->updateBoundingBox();
  root->addItemDrawable(scene);
  return scene;
}

pcl::PointCloud<pcl::PointXYZINormal>::Ptr
SF_StepDTM::createGroundCloud(CT_ResultGroup* outResult, CT_StandardItemGroup* terrainGrp)
{
  CT_Scene* scene = mergeIndices(outResult, terrainGrp, DEF_IN_GRP_CLUSTER, DEF_IN_CLOUD_SEED);
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr cloud = convert(scene);
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr downscaledCloud = downScale(cloud);
  computeNormals(downscaledCloud);
  return downscaledCloud;
}
