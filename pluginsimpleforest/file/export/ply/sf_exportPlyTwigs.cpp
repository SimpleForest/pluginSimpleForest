/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_exportPlyTwigs.h"

int
SF_ExportPlyTwigs::getNumberOfPoints(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  const auto size = bricks.size();
  return size * 2 * m_resolution;
}

int
SF_ExportPlyTwigs::getNumberOfFaces(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  // acutally the same size as number of points ;
  return getNumberOfPoints(bricks);
}

void
SF_ExportPlyTwigs::writeFaces(QTextStream& outStream, std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  const auto size = bricks.size();
  for (int i = 0; i < static_cast<int>(size); i++) {
    int x = 2 * i;
    for (int j = 0; j < m_resolution; j++) {
      outStream << "3 ";
      outStream << QString::number(x * m_resolution + j % m_resolution);
      outStream << " ";
      outStream << QString::number(x * m_resolution + (j + 1) % m_resolution);
      outStream << " ";
      outStream << QString::number((x + 1) * m_resolution + (j + 1) % m_resolution);
      outStream << "\n";

      outStream << "3 ";
      outStream << QString::number((x + 1) * m_resolution + (j + 1) % m_resolution);
      outStream << " ";
      outStream << QString::number((x + 1) * m_resolution + j % m_resolution);
      outStream << " ";
      outStream << QString::number(x * m_resolution + j % m_resolution);
      outStream << "\n";
    }
  }
}

pcl::PointCloud<pcl::PointXYZI>::Ptr
SF_ExportPlyTwigs::bricksToCloud(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
  for (std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick : bricks) {
    pcl::PointCloud<pcl::PointXYZI>::Ptr brickCloud = brickToCloud(brick);
    *cloud += *brickCloud;
  }
  return cloud;
}

void
SF_ExportPlyTwigs::exportQSM(QString path,
                             QString qsmName,
                             std::shared_ptr<SF_ModelQSM> qsm,
                             SF_ExportPlyPolicy exportPolicy,
                             int resolution)
{
  m_qsm = qsm;
  m_exportPolicy = exportPolicy;

  QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
  QString fullPath = getFullPath(path);
  fullPath.append("twigs/");
  QDir dir(fullPath);
  if (!dir.exists())
    dir.mkpath(".");
  QString fullName = getFullName(qsmName, "Twig.ply");
  fullPath.append(fullName);
  QFile file(fullPath);

  m_exportPolicy = SF_ExportPlyPolicy::STEM;
  m_resolution = resolution;
  auto bricksall = m_qsm->getBuildingBricks();
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks;
  std::copy_if(bricksall.begin(),
               bricksall.end(),
               std::back_inserter(bricks),
               [](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick) { return brick->getSegment()->isLeave(); });
  try {
    getMinMax(bricks);
  } catch (const std::exception& e) {
    std::string errorString = "[SF_ExportPly] : getMinMax() did throw:" + std::string(e.what());
    std::cout << errorString << std::endl;
  } catch (...) {
    std::string errorString = "[SF_ExportPly] : getMinMax() did throw and unknown exception.";
    std::cout << errorString << std::endl;
  }
  if (file.open(QIODevice::WriteOnly)) {
    QTextStream outStream(&file);
    writeHeader(outStream, bricks);
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud = bricksToCloud(bricks);
    writeCloud(outStream, cloud);
    writeFaces(outStream, bricks);
    file.close();
  }
}
