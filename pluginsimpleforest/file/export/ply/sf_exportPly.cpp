/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_exportPly.h"

#include <qsm/algorithm/postprocessing/sf_mergeonechildsegments.h>

int
SF_ExportPly::getNumberOfPoints(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  return bricks.size() * 2 * m_resolution;
}

int
SF_ExportPly::getNumberOfFaces(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  // acutally the same size as number of points;
  return getNumberOfPoints(bricks);
}

void
SF_ExportPly::writeFaces(QTextStream& outStream, std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  for (int i = 0; i < static_cast<int>(bricks.size()); i++) {
    int x = 2 * i;
    for (int j = 0; j < m_resolution; j++) {
      outStream << "3 ";
      outStream << QString::number(x * m_resolution + j % m_resolution);
      outStream << " ";
      outStream << QString::number(x * m_resolution + (j + 1) % m_resolution);
      outStream << " ";
      outStream << QString::number((x + 1) * m_resolution + (j + 1) % m_resolution);
      outStream << "\n";

      outStream << "3 ";
      outStream << QString::number((x + 1) * m_resolution + (j + 1) % m_resolution);
      outStream << " ";
      outStream << QString::number((x + 1) * m_resolution + j % m_resolution);
      outStream << " ";
      outStream << QString::number(x * m_resolution + j % m_resolution);
      outStream << "\n";
    }
  }
}

pcl::PointCloud<pcl::PointXYZI>::Ptr
SF_ExportPly::bricksToCloud(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks)
{
  pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
  for (std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick : bricks) {
    pcl::PointCloud<pcl::PointXYZI>::Ptr brickCloud = brickToCloud(brick);
    *cloud += *brickCloud;
  }
  return cloud;
}

void
SF_ExportPly::exportAllometryDetails(QString path, QString qsmName, double targetRadius)
{
  qsmName.append(QString::number(targetRadius));
  auto bricksAll = m_qsm->getBuildingBricks();
  auto minIt = std::min_element(
    bricksAll.begin(),
    bricksAll.end(),
    [&targetRadius](std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick1, std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick2) {
      return std::abs(brick1->getRadius() - targetRadius) < std::abs(brick2->getRadius() - targetRadius);
    });
  {
    QString fullPath = getFullPath(path);
    QDir dir(fullPath);
    if (!dir.exists())
      dir.mkpath(".");
    std::vector<std::shared_ptr<SF_ModelSegment>> firstIteration{ (*minIt)->getSegment() };
    auto pair = std::make_pair(0, firstIteration);
    std::vector<std::pair<int, std::vector<std::shared_ptr<SF_ModelSegment>>>> vector{ pair };
    addChildrenRecursively(vector);
    for (auto pair : vector) {
      auto qsmNameCpy = qsmName;
      auto fullPathCpy = fullPath;
      QString fullName = getFullName(qsmNameCpy + "CylinderLevel" + QString::number(pair.first));
      fullPathCpy.append(fullName);
      QFile file(fullPathCpy);
      std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks;
      for (auto segment : pair.second) {
        auto br = segment->getBuildingBricks();
        bricks.insert(bricks.end(), br.begin(), br.end());
      }
      try {
        getMinMax(bricks);
      } catch (const std::exception& e) {
        std::string errorString = "[SF_ExportPly] : getMinMax() did throw:" + std::string(e.what());
        std::cout << errorString << std::endl;
      } catch (...) {
        std::string errorString = "[SF_ExportPly] : getMinMax() did throw and unknown exception.";
        std::cout << errorString << std::endl;
      }
      if (file.open(QIODevice::WriteOnly)) {
        QTextStream outStream(&file);
        writeHeader(outStream, bricks);
        pcl::PointCloud<pcl::PointXYZI>::Ptr cloud = bricksToCloud(bricks);
        writeCloud(outStream, cloud);
        writeFaces(outStream, bricks);
        file.close();
      }
    }
  }

  {
    QString fullPath = getFullPath(path);
    QDir dir(fullPath);
    if (!dir.exists())
      dir.mkpath(".");
    QString fullName = getFullName(qsmName + "KeyCylinderFront");
    fullPath.append(fullName);
    QFile file(fullPath);
    std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks{ (*minIt)->getSegment()->getBuildingBricks().front() };
    try {
      getMinMax(bricks);
    } catch (const std::exception& e) {
      std::string errorString = "[SF_ExportPly] : getMinMax() did throw:" + std::string(e.what());
      std::cout << errorString << std::endl;
    } catch (...) {
      std::string errorString = "[SF_ExportPly] : getMinMax() did throw and unknown exception.";
      std::cout << errorString << std::endl;
    }
    if (file.open(QIODevice::WriteOnly)) {
      QTextStream outStream(&file);
      writeHeader(outStream, bricks);
      pcl::PointCloud<pcl::PointXYZI>::Ptr cloud = bricksToCloud(bricks);
      writeCloud(outStream, cloud);
      writeFaces(outStream, bricks);
      file.close();
    }
  }

  {
    QString fullPath = getFullPath(path);
    QDir dir(fullPath);
    if (!dir.exists())
      dir.mkpath(".");
    QString fullName = getFullName(qsmName + "KeyCylinderMiddle");
    fullPath.append(fullName);
    QFile file(fullPath);
    auto segs = (*minIt)->getSegment()->getTree()->getSegments((*minIt)->getSegment());
    std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks;
    auto bricksSegment = (*minIt)->getSegment()->getBuildingBricks();
    bricks.push_back(bricksSegment[bricksSegment.size() / 2]);
    try {
      getMinMax(bricks);
    } catch (const std::exception& e) {
      std::string errorString = "[SF_ExportPly] : getMinMax() did throw:" + std::string(e.what());
      std::cout << errorString << std::endl;
    } catch (...) {
      std::string errorString = "[SF_ExportPly] : getMinMax() did throw and unknown exception.";
      std::cout << errorString << std::endl;
    }
    if (file.open(QIODevice::WriteOnly)) {
      QTextStream outStream(&file);
      writeHeader(outStream, bricks);
      pcl::PointCloud<pcl::PointXYZI>::Ptr cloud = bricksToCloud(bricks);
      writeCloud(outStream, cloud);
      writeFaces(outStream, bricks);
      file.close();
    }
  }

  {
    QString fullPath = getFullPath(path);
    QDir dir(fullPath);
    if (!dir.exists())
      dir.mkpath(".");
    QString fullName = getFullName(qsmName + "Branch");
    fullPath.append(fullName);
    QFile file(fullPath);
    auto segs = (*minIt)->getSegment()->getTree()->getSegments((*minIt)->getSegment());
    std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks;
    for (auto seg : segs) {
      auto br = seg->getBuildingBricks();
      bricks.insert(bricks.begin(), br.begin(), br.end());
    }
    auto bricksSegment = (*minIt)->getSegment()->getBuildingBricks();
    bricks.insert(bricks.begin(), std::next(bricksSegment.begin(), bricksSegment.size() / 2), bricksSegment.end());
    try {
      getMinMax(bricks);
    } catch (const std::exception& e) {
      std::string errorString = "[SF_ExportPly] : getMinMax() did throw:" + std::string(e.what());
      std::cout << errorString << std::endl;
    } catch (...) {
      std::string errorString = "[SF_ExportPly] : getMinMax() did throw and unknown exception.";
      std::cout << errorString << std::endl;
    }
    if (file.open(QIODevice::WriteOnly)) {
      QTextStream outStream(&file);
      writeHeader(outStream, bricks);
      pcl::PointCloud<pcl::PointXYZI>::Ptr cloud = bricksToCloud(bricks);
      writeCloud(outStream, cloud);
      writeFaces(outStream, bricks);
      file.close();
    }
  }
}

void
SF_ExportPly::addChildrenRecursively(std::vector<std::pair<int, std::vector<std::shared_ptr<SF_ModelSegment>>>>& segments)
{
  auto lastComputed = segments.back().second;
  auto allLeaves = std::all_of(
    lastComputed.begin(), lastComputed.end(), [](std::shared_ptr<SF_ModelSegment> seg) { return seg->isLeave(); });
  if (allLeaves) {
    return;
  }
  std::vector<std::shared_ptr<SF_ModelSegment>> children;
  for (auto seg : lastComputed) {
    if (seg->isLeave()) {
      children.push_back(seg);
    } else {
      auto segsChildren = seg->getChildren();
      children.insert(children.end(), segsChildren.begin(), segsChildren.end());
    }
  }
  auto pair = std::make_pair(++segments.back().first, std::move(children));
  segments.push_back(std::move(pair));
  addChildrenRecursively(segments);
}

SF_ExportPly::SF_ExportPly() : SF_AbstractExportPly() {}

void
SF_ExportPly::exportQSM(QString path,
                        QString qsmName,
                        std::shared_ptr<SF_ModelQSM> qsm,
                        SF_ExportPlyPolicy exportPolicy,
                        int resolution)
{
  SF_MergeOneChildSegments moc;
  moc.compute(qsm);
  m_qsm = qsm;
  m_exportPolicy = exportPolicy;

  QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
  auto pathCpy = path;
  QString fullPath = getFullPath(path);
  QDir dir(fullPath);
  if (!dir.exists())
    dir.mkpath(".");
  QString fullName = getFullName(qsmName);
  fullPath.append(fullName);
  QFile file(fullPath);

  m_resolution = resolution;
  auto bricks = m_qsm->getBuildingBricks();
  try {
    getMinMax(bricks);
  } catch (const std::exception& e) {
    std::string errorString = "[SF_ExportPly] : getMinMax() did throw:" + std::string(e.what());
    std::cout << errorString << std::endl;
  } catch (...) {
    std::string errorString = "[SF_ExportPly] : getMinMax() did throw and unknown exception.";
    std::cout << errorString << std::endl;
  }
  if (file.open(QIODevice::WriteOnly)) {
    QTextStream outStream(&file);
    writeHeader(outStream, bricks);
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud = bricksToCloud(bricks);
    writeCloud(outStream, cloud);
    writeFaces(outStream, bricks);
    file.close();
  }
}
