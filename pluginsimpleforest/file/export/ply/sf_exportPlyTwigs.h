/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_EXPORTPLYTWIGS_H
#define SF_EXPORTPLYTWIGS_H

#include "file/export/ply/sf_abstractExportPly.h"

class SF_ExportPlyTwigs : public SF_AbstractExportPly
{
  virtual int getNumberOfPoints(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks) override;
  virtual int getNumberOfFaces(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks) override;
  void writeFaces(QTextStream& outStream, std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks) override;
  virtual pcl::PointCloud<pcl::PointXYZI>::Ptr bricksToCloud(
    std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks) override;

public:
  SF_ExportPlyTwigs() = default;
  virtual void exportQSM(QString path,
                         QString qsmName,
                         std::shared_ptr<SF_ModelQSM> qsm,
                         SF_ExportPlyPolicy exportPolicy,
                         int resolution = 8) override;
};

#endif // SF_EXPORTPLYTWIGS_H
