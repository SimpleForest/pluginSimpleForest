/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_unittestSegment.h"

void
SF_UnitTestSegment::remove()
{
  std::shared_ptr<SF_ModelSegment> parent(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child1(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child2(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child1Child1(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child1Child2(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> child2Child1(new SF_ModelSegment);
  parent->addChild(child1);
  parent->addChild(child2);
  child1->addChild(child1Child1);
  child1->addChild(child1Child2);
  child2->addChild(child2Child1);
  const auto sizeFirstChild = static_cast<std::uint32_t>(parent->getChildren().front()->getChildren().size());
  QCOMPARE(sizeFirstChild, static_cast<std::uint32_t>(2));
  child1->remove();
  {
    const auto sizeParent = static_cast<std::uint32_t>(parent->getChildren().size());
    QCOMPARE(sizeParent, static_cast<std::uint32_t>(1));
    const auto sizeFirstChild = static_cast<std::uint32_t>(parent->getChildren().front()->getChildren().size());
    QCOMPARE(sizeFirstChild, static_cast<std::uint32_t>(1));
  }
  child2->remove();
  {
    const auto sizeParent = static_cast<std::uint32_t>(parent->getChildren().size());
    QCOMPARE(sizeParent, static_cast<std::uint32_t>(0));
  }
}
