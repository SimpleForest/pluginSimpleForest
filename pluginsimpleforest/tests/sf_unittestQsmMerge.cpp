/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_unittestQsmMerge.h"

#include "qsm/algorithm/postprocessing/sf_mergeQsm.h"
#include "qsm/model/sf_modelCylinderBuildingbrick.h"

std::shared_ptr<SF_ModelQSM>
SF_UnitTestQsmMerge::parentQsm()
{
  std::shared_ptr<SF_ModelQSM> parent(new SF_ModelQSM);
  std::shared_ptr<SF_ModelSegment> seg1(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> seg11(new SF_ModelSegment);
  std::shared_ptr<SF_ModelSegment> seg12(new SF_ModelSegment);
  parent->setRootSegment(seg1);
  seg1->addChild(seg11);
  seg1->addChild(seg12);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> brick1a(
    new Sf_ModelCylinderBuildingbrick(Eigen::Vector3d(0., 0., 0.), Eigen::Vector3d(0., 0., 1.), 1.));
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> brick1b(
    new Sf_ModelCylinderBuildingbrick(Eigen::Vector3d(0., 0., 1.), Eigen::Vector3d(0., 0., 2.), 1.));
  seg1->addBuildingBrick(brick1a);
  seg1->addBuildingBrick(brick1b);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> brick11a(
    new Sf_ModelCylinderBuildingbrick(Eigen::Vector3d(0., 0., 2.), Eigen::Vector3d(0., 0., 3.), 1.));
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> brick11b(
    new Sf_ModelCylinderBuildingbrick(Eigen::Vector3d(0., 0., 3.), Eigen::Vector3d(0., 0., 4.), 1.));
  seg11->addBuildingBrick(brick11a);
  seg12->addBuildingBrick(brick11b);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> brick12a(
    new Sf_ModelCylinderBuildingbrick(Eigen::Vector3d(0., 0., 2.), Eigen::Vector3d(0., 1., 2.), 1.));
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> brick12b(
    new Sf_ModelCylinderBuildingbrick(Eigen::Vector3d(0., 1., 2.), Eigen::Vector3d(0., 2., 2.), 1.));
  seg12->addBuildingBrick(brick12a);
  seg12->addBuildingBrick(brick12b);
  return parent;
}

std::shared_ptr<SF_ModelQSM>
SF_UnitTestQsmMerge::childQsm()
{
  std::shared_ptr<SF_ModelQSM> child(new SF_ModelQSM);
  std::shared_ptr<SF_ModelSegment> seg1(new SF_ModelSegment);
  child->setRootSegment(seg1);
  std::shared_ptr<Sf_ModelCylinderBuildingbrick> brick1a(
    new Sf_ModelCylinderBuildingbrick(Eigen::Vector3d(1., 0., 1.), Eigen::Vector3d(2., 0., 1.), 1.));
  seg1->addBuildingBrick(brick1a);
  return child;
}

std::shared_ptr<SF_ModelQSM>
SF_UnitTestQsmMerge::merge(std::shared_ptr<SF_ModelQSM> qsm1, std::shared_ptr<SF_ModelQSM> qsm2)
{
  SF_MergeQsm mergeQsm;
  return mergeQsm.merge(qsm1, qsm2);
}

void
SF_UnitTestQsmMerge::test(std::shared_ptr<SF_ModelQSM> qsm)
{
  auto root = qsm->getRootSegment();
  QCOMPARE(root->getStart(), Eigen::Vector3d(0., 0., 0.));
  QCOMPARE(root->getEnd(), Eigen::Vector3d(0., 0., 1.));

  // It should be split after first cylinder
  QCOMPARE(root->getBuildingBricks().size(), static_cast<size_t>(1));
  // 6 cylinders have been merged with 1 clyinder via 1 connection cylinder. All of length 1.
  QCOMPARE(root->getGrowthLength(), 8.);
  // 3 segments of parent have been split into 4, and then 1 was added
  auto segments = qsm->getSegments();
  QCOMPARE(segments.size(), static_cast<size_t>(5));
  QVERIFY(
    std::all_of(segments.begin(), segments.end(), [qsm](std::shared_ptr<SF_ModelSegment> seg) { return seg->getTree() == qsm; }));
}

void
SF_UnitTestQsmMerge::mergeWorks()
{
  auto qsm = merge(parentQsm(), childQsm());
  test(qsm);
}

void
SF_UnitTestQsmMerge::mergeWorksWithSwap()
{
  auto qsm = merge(childQsm(), parentQsm());
  test(qsm);
}

void
SF_UnitTestQsmMerge::nullptrAsParentIsAccepted()
{
  auto qsm = merge(parentQsm(), childQsm());
  auto qsm2 = merge(nullptr, qsm);
  test(qsm2);
}

void
SF_UnitTestQsmMerge::nullptrAsChildIsAccepted()
{
  auto qsm = merge(parentQsm(), childQsm());
  auto qsm2 = merge(qsm, nullptr);
  test(qsm2);
}
