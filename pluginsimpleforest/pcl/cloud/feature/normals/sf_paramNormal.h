/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_PARAMNORMAL_H
#define SF_PARAMNORMAL_H

struct SF_ParamNormal
{
  int m_numberOfNeighbors = 5;
  bool m_useRange = true;
  float m_radius = 0.03f;

  std::ostream& operator<<(std::ostream& stream, const SF_ParamNormal& matrix)
  {
    stream << "For the cloud the normals have been computed with a";
    if (m_useRange) {
      stream << " range search with" << std::endl;
      strean << "[" << std::endl;
      stream << "radius = " << m_range << std::endl;
      stream << "]" << std::endl;
    } else {
      stream << " nearest neighbor search with" << std::endl;
      strean << "[" << std::endl;
      stream << "number of neighors = " << m_numberOfNeighbors << std::endl;
      stream << "]" << std::endl;
    }
    stream << "." << std::endl;
  }
};

#endif // SF_PARAMNORMAL_H
