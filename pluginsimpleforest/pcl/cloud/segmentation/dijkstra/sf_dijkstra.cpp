#ifndef SF_DIJKSTRA_HPP
#define SF_DIJKSTRA_HPP

#include "sf_dijkstra.h"

#include <pcl/search/kdtree.h>

SF_Dijkstra::SF_Dijkstra(SF_CloudNormal::Ptr cloudIn, SF_CloudNormal::Ptr cloudInSeeds, float range, bool useFixedDistance)
  : _cloudIn(cloudIn), _cloudInSeeds(cloudInSeeds), _range(range), m_useFixDistance(useFixedDistance)
{
  initialize();
  compute();
}

std::vector<float>
SF_Dijkstra::getDistances() const
{
  return _distances;
}

void
SF_Dijkstra::transferIntensity()
{
  pcl::search::KdTree<SF_PointNormal>::Ptr kdtree(new pcl::search::KdTree<SF_PointNormal>);
  kdtree->setInputCloud(_cloudInSeeds);
  size_t size = _cloudIn->points.size();
  float sqrdRange = (m_useFixDistance) ? 0.0000001 : _range * _range * 0.1f;
  for (size_t i = 0; i < size; i++) {
    SF_PointNormal point = _cloudIn->points[i];
    std::vector<int> pointIdxNKNSearch(1);
    std::vector<float> pointNKNSquaredDistance(1);
    if (kdtree->nearestKSearch(point, 1, pointIdxNKNSearch, pointNKNSquaredDistance) > 0) {
      if (pointNKNSquaredDistance[0] < sqrdRange) {
        int index = _cloudInSeeds->points[pointIdxNKNSearch[0]].intensity;
        _cloudIn->points[i].intensity = index;
        _cloudIn->points[i].normal_x = 0;
        _cloudIn->points[i].normal_y = 0;
        _cloudIn->points[i].normal_z = 1;
        _distances.push_back(0);
      } else {
        _cloudIn->points[i].intensity = -1;
        _distances.push_back(MAXDISTANCE);
        _cloudIn->points[i].normal_x = 0;
        _cloudIn->points[i].normal_y = 1;
        _cloudIn->points[i].normal_z = 0;
      }
    } else {
      _cloudIn->points[i].intensity = -1;
      _distances.push_back(MAXDISTANCE);
      _cloudIn->points[i].normal_x = 0;
      _cloudIn->points[i].normal_y = 1;
      _cloudIn->points[i].normal_z = 0;
    }
  }
}

void
SF_Dijkstra::initializeHeap()
{
  _priorityQueue.clear();
  size_t size = _cloudIn->points.size();
  _points.resize(size);
  _handle.clear();
  for (size_t i = 0; i < size; i++) {
    Point point(_cloudIn->points[i], _distances[i]);
    _points[i] = point;
    Heap::handle_type h = _priorityQueue.push(point);
    _handle.push_back(h);
    (*h).handle = h;
  }
}

void
SF_Dijkstra::initializeKDTree()
{
  _kdtree.reset(new pcl::KdTreeFLANN<SF_PointNormal>());
  _kdtree->setInputCloud(_cloudIn);
}

SF_CloudNormal::Ptr
SF_Dijkstra::getCloudIn() const
{
  return _cloudIn;
}

void
SF_Dijkstra::initialize()
{
  _parentIndices.resize(_cloudIn->points.size());
  std::fill(_parentIndices.begin(), _parentIndices.end(), -1);
  transferIntensity();
  initializeHeap();
  initializeKDTree();
}

int
SF_Dijkstra::getIndex(const SF_PointNormal& point)
{
  std::vector<int> indices(1);
  std::vector<float> sqrtDistances(1);
  _kdtree->nearestKSearch(point, 1, indices, sqrtDistances);
  return indices[0];
}

std::vector<int>
SF_Dijkstra::getNeighbors(const SF_PointNormal& point)
{
  std::vector<int> indices;
  std::vector<float> sqrtDistances;
  std::vector<int> result;
  _kdtree->radiusSearch(point, _range, indices, sqrtDistances);
  for (size_t i = 0; i < indices.size(); i++) {
    int index = indices[i];
    if (!_points[index]._visited) {
      result.push_back(index);
    }
  }
  return result;
}

std::vector<int>
SF_Dijkstra::getParentIndices() const
{
  return _parentIndices;
}

float
SF_Dijkstra::getDistance(const SF_PointNormal& p1, const SF_PointNormal& p2)
{
  float dx, dy, dz;
  dx = p1.x - p2.x;
  dy = p1.y - p2.y;
  dz = p1.z - p2.z;
  return (std::sqrt(dx * dx + dy * dy + dz * dz));
}

void
SF_Dijkstra::compute()
{
  _maxDistance = 0;
  while (!_priorityQueue.empty()) {
    Point pointStruct = _priorityQueue.top()._point;
    SF_PointNormal point = pointStruct._point;
    int index = getIndex(point);
    if (pointStruct._distance == MAXDISTANCE) {
      break;
    }
    _priorityQueue.pop();
    _points[index]._visited = true;
    _distances[index] = pointStruct._distance;
    std::vector<int> neighbors = getNeighbors(point);
    for (size_t i = 0; i < neighbors.size(); i++) {
      int indexNeighbor = neighbors[i];
      Point neighbor = (*_handle[indexNeighbor])._point;
      float distBetween = getDistance(point, neighbor._point);
      if (pointStruct._distance + distBetween < neighbor._distance) {
        _parentIndices[indexNeighbor] = index;
        float d = pointStruct._distance + distBetween;
        if (_maxDistance < d && d != MAXDISTANCE) {
          _maxDistance = d;
        }
        (*_handle[indexNeighbor])._point._distance = d;
        (*_handle[indexNeighbor])._point._point.intensity = pointStruct._point.intensity;
        _cloudIn->points[indexNeighbor].intensity = pointStruct._point.intensity;
        _cloudIn->points[indexNeighbor].normal_x = _cloudIn->points[indexNeighbor].x - point.x;
        _cloudIn->points[indexNeighbor].normal_y = _cloudIn->points[indexNeighbor].y - point.y;
        _cloudIn->points[indexNeighbor].normal_z = _cloudIn->points[indexNeighbor].z - point.z;
        _priorityQueue.decrease(_handle[indexNeighbor]);
      }
    }
  }
}

float
SF_Dijkstra::getMaxDistance() const
{
  return _maxDistance;
}

#endif // SF_DIJKSTRA_HPP
